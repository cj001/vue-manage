import { RouteRecordRaw } from 'vue-router'
import { IBreadcrumb } from '@/base-ui/breadcrumb'

let firstMenu: any = null // 用来保留第一个路由 用于main的默认跳转
export function mapMenusToRoutes(userMenus: any[]): RouteRecordRaw[] {
  const routes: RouteRecordRaw[] = []
  // 1.加载默认所有的routes
  const allRoutes: RouteRecordRaw[] = []
  // require.context webpack里面的工具 帮助我们加载某个文件夹
  // 参数1 文件夹目录 参数2 是否递归查询（false的时候只查找当前文件夹的内容） 参数3 需要查找对策文件后缀名
  const routeFiles = require.context('../router/main', true, /\.ts/)
  // routeFiles.keys() 获取所有的文件的路径（相对于参数1中的路径 相对路径）
  routeFiles.keys().forEach((key) => {
    // 例： key = ./system/usr/user.ts
    const route = require('../router/main' + key.split('.')[1])
    allRoutes.push(route.default)
  })

  // 2.根据获取的菜单获取需要添加的routes
  // userMenus:
  // type === 1 -> children -> type === 1
  // type === 2 -> url -> route
  const _resourceGetRoute = (menus: any[]) => {
    for (const menu of menus) {
      if (menu.type === 2) {
        const route = allRoutes.find((route) => route.path === menu.url)
        if (route) {
          routes.push(route)
        }
        if (!firstMenu) {
          firstMenu = menu
        }
      } else {
        _resourceGetRoute(menu.children)
      }
    }
  }
  _resourceGetRoute(userMenus)
  return routes
}

export function pathMapToMenu(
  userMenus: any[],
  path: string,
  breadcrumbs?: IBreadcrumb[]
): any {
  for (const menu of userMenus) {
    if (menu.type === 1) {
      const targetMenu = pathMapToMenu(menu.children ?? [], path)
      if (targetMenu) {
        breadcrumbs?.push({ name: menu.name })
        breadcrumbs?.push({ name: targetMenu.name })
        return targetMenu
      }
    } else if (menu.type === 2 && menu.url === path) {
      return menu
    }
  }
}

export function pathMapBreadcrumbs(userMenus: any[], currentPath: string) {
  const breadcrumbs: IBreadcrumb[] = []
  pathMapToMenu(userMenus, currentPath, breadcrumbs)
  return breadcrumbs
}

export function mapMenusToPermissions(userMenus: any[]) {
  const permissions: string[] = []

  const _recurseGetPermission = (menus: any[]) => {
    for (const menu of menus) {
      if (menu.type === 1 || menu.type === 2) {
        _recurseGetPermission(menu.children ?? [])
      } else if (menu.type === 3) {
        permissions.push(menu.permission)
      }
    }
  }
  _recurseGetPermission(userMenus)

  return permissions
}

export function menuMapLeafKeys(menuList: any[]) {
  const leftKeys: number[] = []

  const _recurseGetLeaf = (menuList: any[]) => {
    for (const menu of menuList) {
      if (menu.children) {
        _recurseGetLeaf(menu.children)
      } else {
        leftKeys.push(menu.id)
      }
    }
  }
  _recurseGetLeaf(menuList)

  return leftKeys
}

export { firstMenu }
