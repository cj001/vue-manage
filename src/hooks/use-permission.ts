import { useStore } from '@/store'
// pageName 页面名称  handleName当前页面中想要验证的权限
export function usePermission(pageName: string, handleName: string) {
  const store = useStore()
  const permissions = store.state.login.permissions
  const verifyPermission = `system:${pageName}:${handleName}`

  // name = "coderwhy"
  // !name -> false
  // !!name -> true
  // string 转boolean
  return !!permissions.find((item) => item === verifyPermission)
}
