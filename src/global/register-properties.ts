import { App } from 'vue'
import { formatUtcString } from '@/utils/date-format'

export default function registerProperties(app: App) {
  app.config.globalProperties.$filter = {
    foo() {
      console.log('foo')
    },
    // 应用举例 <span>{{ $filters.formatTime(scope.row.createAt) }}</span>
    formatTime(date: string) {
      return formatUtcString(date)
    }
  }
}
