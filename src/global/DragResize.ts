import { App } from 'vue'
import VueDraggableResizable from 'vue-draggable-resizable-next'

export function initDrag(app: App): void {
  app.component('VueDraggableResizable', VueDraggableResizable)
}
