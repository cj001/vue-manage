import { App } from 'vue'
import 'element-plus/theme-chalk/base.css'

import {
  ElAvatar,
  ElButton,
  ElCheckbox,
  ElContainer,
  ElDatePicker,
  ElDropdown,
  ElForm,
  ElFormItem,
  ElInput,
  ElLink,
  ElRadio,
  ElRow,
  ElCol,
  ElSelect,
  ElTabs,
  ElTabPane,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElTable,
  ElTableColumn,
  ElPagination,
  ElImage,
  ElImageViewer,
  ElDialog,
  ElTree,
  ElCard,
  ElAside,
  ElHeader,
  ElMain,
  ElMenu,
  ElMenuItem,
  ElSubMenu,
  ElIcon,
  ElDropdownMenu,
  ElDropdownItem,
  ElOption,
  ElConfigProvider
} from 'element-plus/lib/components'

// element-plus 之前的版本 from 'element-plus'就可以了 后面的版本文件位置做了改变
// import { ElFormItem } from 'element-plus/lib/components'
// import { ElTabPane } from 'element-plus/lib/components'

const components = [
  ElAvatar,
  ElButton,
  ElCheckbox,
  ElContainer,
  ElDatePicker,
  ElDropdown,
  ElForm,
  ElFormItem,
  ElInput,
  ElLink,
  ElRadio,
  ElRow,
  ElCol,
  ElSelect,
  ElTabs,
  ElTableColumn,
  ElTabPane,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElTable,
  ElPagination,
  ElImage,
  ElImageViewer,
  ElDialog,
  ElTree,
  ElCard,
  ElAside,
  ElHeader,
  ElMain,
  ElMenu,
  ElMenuItem,
  ElSubMenu,
  ElIcon,
  ElDropdownMenu,
  ElDropdownItem,
  ElOption,
  ElConfigProvider
]

// const aaa = function (app: App): void {
//   for (const item of components) {
//     app.component(item.name, item)
//   }
// }
//
// export default aaa

export default function (app: App): void {
  for (const item of components) {
    app.component(item.name, item)
  }
}

// 模块化相关联系
// const aaa = function (app: App): void {
//   for (const item of components) {
//     app.component(item.name, item)
//   }
// }

// export function testA(app: App): void {
//   console.log(app)
// }
//
// export default function testCj(app: App): void {
//   console.log(111)
// }
