import registerProperties from './register-properties'
import { App } from 'vue'
import registerElement from './register-element'
// 注册可拖拽组件
import { initDrag } from '@/global/DragResize'

export function globalRegister(app: App): void {
  app.use(registerProperties)
  app.use(registerElement)
  initDrag(app)
  // registerElement(app) // 这种写法也是可以的
}
