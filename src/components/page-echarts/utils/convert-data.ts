// 城市经纬度数据
import { coordinateData } from './coordinate-data'

export const convertData = function (data: any) {
  const res = []
  for (let i = 0; i < data.length; i++) {
    // 南京: [118.78, 32.04]
    const geoCoord = coordinateData[data[i].name]
    if (geoCoord) {
      res.push({
        name: data[i].name,
        // 先取到南京的经纬度  匹配我们传递的数据  将该地址的销量信息放在后面
        // { address: "南京", count: 55683 }
        // [120.13, 33.38, 55683]
        // concat链接两个或多个数组
        value: geoCoord.concat(data[i].value)
      })
      // 例： { name: '南京', value: [120.13, 33.38, 55683] }
    }
  }
  return res
}
