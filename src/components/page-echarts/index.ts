import Bar from './src/bar.vue'
import cjLine from './src/cjLine.vue'
import cjMap from './src/cjMap.vue'
import Pie from './src/pie.vue'
import Rose from './src/rose.vue'

export { Bar, cjLine, cjMap, Pie, Rose }
