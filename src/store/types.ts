import { ILoginState } from './login/types'
import { ISystemState } from '@/store/main/system/types'
import { IDashboardState } from '@/store/main/analysis/types'

export interface IRootState {
  name: string
  age: number
  // height: number
  entireDepartment: any[]
  entireRole: any[]
  entireMenu: any[]
}
// 带有login module的类型（ILoginState）
export interface IRootWithModule {
  login: ILoginState
  system: ISystemState
  dashboard: IDashboardState
}

// IStoreType类型既有login模块的类型 又可以有一些其他的类型（IRootState）
// 其他模块的类型如果有需要 继续使用交叉类型即可
export type IStoreType = IRootState & IRootWithModule
