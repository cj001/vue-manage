// 防止useStore和后面导出的方法重名 因此设置了别名(useVuexStore)
import { createStore, Store, useStore as useVuexStore } from 'vuex'

import login from './login/login'
import system from '@/store/main/system/system'
import dashboard from '@/store/main/analysis/dashboard'

import { IRootState, IStoreType } from './types'
import localCache from '@/utils/cache'
import { getPageListData } from '@/service/main/system/system'

// IRootState中的类型有什么 创建的store里面有可以有哪些state
const store = createStore<IRootState>({
  state() {
    // 将一下对象返回给了state
    return {
      name: 'cj001',
      age: 18,
      height: '123',
      entireDepartment: [],
      entireRole: [],
      entireMenu: []
    }
  },
  mutations: {
    changeName(state) {
      console.log(state.name)
      console.log(state.age)
      // height虽然在state中有赋值 但是 在IRootState没有定义 所以推导不出来
      // console.log(state.height)
    },
    changeEntireDepartment(state, payload) {
      state.entireDepartment = payload
    },
    changeEntireRole(state, payload) {
      state.entireRole = payload
    },
    changeEntireMenu(state, list) {
      state.entireMenu = list
    }
  },
  getters: {},
  actions: {
    async getInitialDataAction({ commit }) {
      const departmentResult = await getPageListData('/department/list', {
        offset: 0,
        size: 1000
      })

      // { list: departmentList } 解构并起别名
      const { list: departmentList } = departmentResult.data

      const roleResult = await getPageListData('/role/list', {
        offset: 0,
        size: 1000
      })

      const { list: roleList } = roleResult.data

      const menuResult = await getPageListData('/menu/list', {})
      const { list: menuList } = menuResult.data

      commit('changeEntireDepartment', departmentList)
      commit('changeEntireRole', roleList)
      commit('changeEntireMenu', menuList)
    }
  },
  modules: {
    login,
    system,
    dashboard
  }
})

// name 和age可以被推导出来
// store.state.name
// store.state.age

// export function setupStore() {
//   store.dispatch('login/loadLocalLogin')
// }

export default store
// 自己定义的Store
export function setupStore() {
  // 可能会有异步请求问题 没有拿到token然后就开始调用initial的方法 所以初始化的方法需要挪个位置
  // getInitialDateAction在login拿到了token后进行操作 和页面刷新的时候 从缓存中获取token后调用它
  // 这个逻辑更为严谨
  store.dispatch('login/loadLocalLogin').then()
  // store.dispatch('getInitialDateAction').then()
}

export function useStore(): Store<IStoreType> {
  return useVuexStore()
}
