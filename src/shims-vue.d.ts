/* eslint-disable */
/*
shims 垫片 给项目做一些垫片  vue文件类型声明
将.vue文件声明成为模块 DefineComponent<{}, {}, any>类型的  export 返回的是根据组件描述，
使用该组件时会根据它创建的组件的实例
vue文件中传入的DefineComponent 会给我们vue中的代码添加提示信息 哪些能传哪些不能传 传了会报错 都会有说提示信息
defineComponent 中定义了许多泛型（有默认值的）
defineComponent对vue中的export的内容做了包裹 帮助我们更好的编写vue代码（可以类型推导功能） 类型不能乱写了
例如：vue2中的props只要没有语法错误可以随便写，但最后编译会报错
添加了类型限制后  写的props必须是{}类型的数据， 有一定的限制作用，并且会根据默认的泛型提供提示信息
*/
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}
declare let $store: any

// 声明json文件
declare module '*.json'
