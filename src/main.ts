import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import 'ant-design-vue/dist/antd.css'
import Antd from 'ant-design-vue'
import { globalRegister } from '@/global'

// 全局引入的方法  比较简单  打出来的包会大一点
// import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// import locale from 'element-plus/lib/locale/lang/zh-cn'
import 'element-plus/theme-chalk/base.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import router from './router'
import store from './store'

import { setupStore } from './store'

// import './service/axios_demo'

import { cjRequest, cjRequest2 } from '@/service'

// 导入初始化样式文件
import 'normalize.css'
import './assets/css/index.less'

const app = createApp(App)

globalRegister(app) // 理解更加直观一点
app
  .use(store)
  // .use(globalRegister) // 使用更符合代码规范 优雅
  // .use(router) // 会导致页面刷新时跳转路径不正确
  // .use(ElementPlus)
  .use(Antd)
setupStore()
app.use(router)

// app.config.globalProperties.$filters = {
//   foo() {
//     console.log('foo')
//   },
//   formatTime(value: string) {
//     return '20222-06-18 08:02:30'
//   }
// }

app.mount('#app')

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

// app.use(router)
// setupStore() 会导致页面刷新时跳转路径不正确
/**
 * 页面刷新时 会重新加载对应的js文件（ts代码最终会转变成js文件）
 main.ts代码由上向下依次执行
 app.use(router)
   1.  会执行router里面的install函数 -》获取当前的path
      -》router.routes里匹配路径（目前还没有由setupStore创建动态路由）
   2. 这里是没有找到我们根据后台返回数据创建的动态路由，没有匹配到对应的路径 所以to的路径是notfound
   3. 导航守卫执行回调函数的时候 由于之前没有匹配到to的路径 页面将跳转至404错误
   4. 所以正确的做法是 先创建动态路由（setupStore()） app.use(router)时执行里面的install函数
      根据path匹配路由 再跳转至正确的页面
 setupStore() // 开始注册动态路由

 所以两行代码的顺序需要调整
 */

// // app.use 相关说明
// // 传入函数  默认会把app作为参数传进去执行该方法
// app.use(function(app: App) {
//   ...
// })
// // 传入的是对象会执行对象中的install方法 执行install的时候 也会默认传入参数app
// app.use({
//   install: function(app: App) {
//     ...
//   }
// })

// cjRequest.request({
//   url: '/home/multidata',
//   method: 'GET'
// })

// cjRequest
//   .cjRequestTest({
//     url: '/home/multidata',
//     method: 'GET',
//     showLoading: true,
//     testCj: '000',
//     interceptors: {
//       requestInterceptor: (config) => {
//         // console.log('单独请求的config')
//         // console.log(config)
//         // console.log(config.testCj) // 错误不可用
//         return config
//       },
//       responseInterceptor: (res) => {
//         // console.log('单独响应的res')
//         // console.log(res.data)
//         // res.info = '001'
//         // console.log(111)
//         // console.log(res)
//         return res
//       }
//     }
//   })
//   .then((res) => {
//     // console.log(res)
//   })

// cjRequest2.request({
//   url: '/home/multidata',
//   method: 'GET'
// })

// interface DataType {
//   data: any
//   returnCode: string
//   success: boolean
// }
//
// cjRequest
//   .cjRequestTest<DataType>({
//     // method: 'GET',
//     url: '/home/multidata',
//     showLoading: true
//   })
//   .then((res) => {
//     console.log(res)
//     console.log(res.data)
//     console.log(res.returnCode)
//     console.log(res.success)
//   })
