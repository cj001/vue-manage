import axios, { AxiosResponse } from 'axios'
import type { AxiosInstance, AxiosRequestConfig } from 'axios'

import { ElLoading } from 'element-plus/lib/components'
import { LoadingInstance } from 'element-plus/lib/components/loading/src/loading'

import { CJRequestInterceptors, CJRequestConfig } from './type'
import { createLogger } from 'vuex'
const DEFAULT_LOADING = true

// interface CJRequestInterceptors<T = AxiosResponse> {
//   requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig
//   requestInterceptorCatch?: (error: any) => any
//   responseInterceptor?: (res: T) => T
//   responseInterceptorCatch?: (error: any) => any
// }.
//
// interface CJRequestConfig<T = AxiosResponse> extends AxiosResponse {
//   interceptors?: CJRequestInterceptors<T>
//   showLoading?: boolean
// }

class CjRequest {
  instance: AxiosInstance
  interceptors?: CJRequestInterceptors
  showLoading: boolean
  loading?: LoadingInstance
  testCj?: string
  // 封装前传入的参数
  // constructor(config: AxiosRequestConfig) {
  //   this.instance = axios.create(config)
  //   this.showLoading = config.showLoading?? DEFAULT_LOADING
  // }

  // 封装拦截器时定义的构造器
  constructor(config: CJRequestConfig) {
    this.instance = axios.create(config)
    this.interceptors = config.interceptors
    this.showLoading = config.showLoading ?? DEFAULT_LOADING
    this.testCj = config.testCj

    // 使用拦截器
    // 1.从config中取出的拦截器(当前实例定义的拦截器)是对应的实例的拦截器
    this.instance.interceptors.request.use(
      this.interceptors?.requestInterceptor,
      this.interceptors?.responseInterceptorCatch
    )
    this.instance.interceptors.response.use(
      this.interceptors?.responseInterceptor,
      this.interceptors?.responseInterceptorCatch
    )

    // 2.添加所有的实例都有的拦截器
    this.instance.interceptors.request.use(
      (config) => {
        // console.log('所有实例都会有的拦截器')
        // console.log(111)
        if (this.showLoading) {
          this.loading = ElLoading.service({
            lock: true,
            text: '数据加载中',
            background: 'rgba(0, 0, 0, 0.5)'
          })
        }
        return config
      },
      (err) => {
        // console.log('所有的实例都有的拦截器: 请求失败拦截')
        // console.log(222)
        return err
      }
    )

    this.instance.interceptors.response.use(
      (res) => {
        // console.log('所有实例都会有的响应器')
        // console.log(333)

        // 测试用延迟2秒关闭loading
        // setTimeout(() => {
        //   this.loading?.close()
        // }, 2000)

        this.loading?.close()

        // console.log(res)
        const data = res.data
        // console.log(data)
        if (data.returnCode === '-1001') {
          // console.log('请求失败~, 错误信息')
        } else {
          return data
        }
      },
      (err) => {
        // console.log(444)
        this.loading?.close()
        if (err.response.status === 404) {
          // console.log('404 error')
        }
        return err
      }
    )
  }

  // 对请求设置拦截器
  // cjRequestTest 封装第一版
  // cjRequestTest(config: CJRequestConfig): void {
  //   if (config.interceptors?.requestInterceptor) {
  //     config = config.interceptors.requestInterceptor(config)
  //   }
  //
  //   // if (config.interceptors?.responseInterceptor) {
  //   //   config = config.interceptors.responseInterceptor(res)
  //   // }
  //
  //   this.instance.request(config).then((res) => {
  //     if (config.interceptors?.responseInterceptor) {
  //       res = config.interceptors?.responseInterceptor(res)
  //       console.log(res)
  //     }
  //   })
  // }

  // 对请求设置拦截器
  cjRequestTest<T = any>(config: CJRequestConfig<T>): Promise<T> {
    // cjRequestTest<T>(config: CJRequestConfig): Promise<T> {
    return new Promise((resolve, reject) => {
      // 1.单个请求对请求config的处理 如果请求中设置了拦截相关的方法 使用config中的拦截器方法
      if (config.interceptors?.requestInterceptor) {
        // 会把config转变后的内容返回给config(在main.ts中调用的config设置的requestInterceptor)
        config.testCj = '001'
        config = config.interceptors.requestInterceptor(config)
      }

      // 2.判断是否需要显示loading
      if (config.showLoading === false) {
        this.showLoading = config.showLoading
      }

      // this.instance.interceptors.request.use(
      //   (res) => console.log(111),
      //   (err) => console.log(222)
      // )
      this.instance
        .request<any, T>(config)
        .then((res) => {
          if (config.interceptors?.responseInterceptor) {
            /* 1.单个请求对数据(响应)的处理
            如果没有(config: CJRequestConfig<T>)处理 那么config.interceptors?.responseInterceptor返回的 默认是AxiosResponse类型
            本案例中希望config.interceptors?.responseInterceptor 返回的是T类型
            （因为之情的请求拦截中已经将res中的data抽离了出来 返回值不可能是AxiosResponse类型）
            所以要做处理
            config.interceptors?.responseInterceptor(res) 就是当前promise的返回值
            */
            res = config.interceptors?.responseInterceptor(res)
            // console.log(res)
          }
          // 2.将showLoading设置true, 这样不会影响下一个请求
          this.showLoading = DEFAULT_LOADING

          // 3.将结果resolve返回出去
          // resolve('success')

          // res.data = 123
          resolve(res)
        })
        .catch((err) => {
          // 将showLoading设置true, 这样不会影响下一个请求
          this.showLoading = DEFAULT_LOADING
          reject(err)
          return err
        })
    })
  }

  request<T>(config: CJRequestConfig<T>): Promise<T> {
    return new Promise((resolve, reject) => {
      // 1.单个请求对请求config的处理
      if (config.interceptors?.requestInterceptor) {
        config = config.interceptors.requestInterceptor(config)
      }

      // 2.判断是否需要显示loading
      if (config.showLoading === false) {
        this.showLoading = config.showLoading
      }

      this.instance
        .request<any, T>(config)
        .then((res) => {
          // 1.单个请求对数据的处理
          if (config.interceptors?.responseInterceptor) {
            res = config.interceptors.responseInterceptor(res)
          }
          // 2.将showLoading设置true, 这样不会影响下一个请求
          this.showLoading = DEFAULT_LOADING

          // 3.将结果resolve返回出去
          resolve(res)
        })
        .catch((err) => {
          // 将showLoading设置true, 这样不会影响下一个请求
          this.showLoading = DEFAULT_LOADING
          reject(err)
          return err
        })
    })
  }

  // get<T>(config: CJRequestConfig): Promise<T> {
  //   return this.cjRequestTest<T>({ ...config, method: 'GET' })
  // }

  get<T = any>(config: CJRequestConfig<T>): Promise<T> {
    return this.cjRequestTest<T>({ ...config, method: 'GET' })
  }

  post<T = any>(config: CJRequestConfig<T>): Promise<T> {
    return this.cjRequestTest<T>({ ...config, method: 'POST' })
  }

  delete<T = any>(config: CJRequestConfig<T>): Promise<T> {
    return this.cjRequestTest<T>({ ...config, method: 'DELETE' })
  }

  patch<T = any>(config: CJRequestConfig<T>): Promise<T> {
    return this.cjRequestTest<T>({ ...config, method: 'PATCH' })
  }
}

export default CjRequest
