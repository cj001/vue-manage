import { AxiosRequestConfig, AxiosResponse } from 'axios'
// AxiosRequestConfig
// 配制拦截器
interface CJRequestInterceptors<T = AxiosResponse> {
  // 请求及回应的成功拦截和发生错误时的拦截
  requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig
  requestInterceptorCatch?: (error: any) => any
  responseInterceptor?: (res: T) => T // 响应返回的类型由外界传入
  responseInterceptorCatch?: (error: any) => any
}

// CJRequestConfig继承AxiosRequestConfig 并添加 拦截器和showLoading（相关扩展参数）
interface CJRequestConfig<T = AxiosResponse> extends AxiosRequestConfig {
  interceptors?: CJRequestInterceptors<T>
  showLoading?: boolean
  testCj?: string
}

export { CJRequestInterceptors, CJRequestConfig }
