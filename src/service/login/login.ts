import { cjRequest } from '@/service'
import { IAccount, IDataType, ILoginResult, IUserInfo } from './types'

enum LoginAPI {
  AccountLogin = '/login',
  LoginUserInfo = '/users/', // 用法: /users/1  'users/'后面接的是id 下面的同理
  UserMenus = '/role/' // 用法: role/1/menu
}

export function accountLoginRequest(account: IAccount) {
  // 给requestUserInfoById接口的返回值设置泛型 否则返回的any（axios封装的时候设置的）
  return cjRequest.post<IDataType<ILoginResult>>({
    url: LoginAPI.AccountLogin,
    data: account
  })
}

export function requestUserInfoById(id: number) {
  // 给requestUserInfoById接口的返回值设置泛型
  // return cjRequest.get<IDataType<IUserInfo>>({
  return cjRequest.get<IDataType>({
    url: LoginAPI.LoginUserInfo + id,
    showLoading: false
  })
}

export function requestUserMenusByRoleId(id: number) {
  return cjRequest.get<IDataType>({
    url: LoginAPI.UserMenus + id + '/menu',
    showLoading: false
  })
}
