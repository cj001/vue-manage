import { requestUserInfoById } from '@/service/login/login'

export interface IAccount {
  name: string
  password: string
}

export interface ILoginResult {
  id: number
  name: string
  token: string
}

export interface IUserInfo {
  name: string
  age: number
  height: number
}

export interface IDataType<T = any> {
  code: number
  data: T
}
