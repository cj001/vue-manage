/*
1. 所有axios实例中设置拦截
2. 特定（设置了拦截器属性的）实例中设置拦截
3. 特定（设置了拦截器属性的）请求设置拦截  很少见
实际中每种类型的请求是不一样的
* */
import CjRequest from '@/service/request'
import { BASE_URL, TIME_OUT } from './request/config'

import localCache from '@/utils/cache'

// 如果开发时需要创建多个axios对象完成不同的请求发送  那么需要创建多个axios实例
const cjRequest = new CjRequest({
  baseURL: BASE_URL,
  timeout: TIME_OUT,
  interceptors: {
    requestInterceptor: (config) => {
      // const token = '111' // 测试用token
      const token = localCache.getCache('token')
      if (token) {
        // @ts-ignore
        config.headers.authorization = `Bearer ${token}`
      }

      // console.log('请求成功拦截')
      return config
    },
    requestInterceptorCatch: (config) => {
      // console.log('请求失败拦截')
      return config
    },
    responseInterceptor: (res) => {
      // console.log('响应成功拦截')
      return res
    },
    responseInterceptorCatch: (res) => {
      // console.log(999)
      // console.log('响应失败拦截')
      return res
    }
  }
})

// 创建的第二个axios实例
const cjRequest2 = new CjRequest({
  baseURL: BASE_URL,
  timeout: TIME_OUT
  // interceptors: {
  //   requestInterceptor: (config) => {
  //     console.log('请求成功拦截02')
  //     return config
  //   },
  //   requestInterceptorCatch: (config) => {
  //     console.log('请求失败拦截02')
  //     return config
  //   },
  //   responseInterceptor: (config) => {
  //     console.log('响应成功拦截02')
  //     return config
  //   },
  //   responseInterceptorCatch: (config) => {
  //     console.log('响应失败拦截02')
  //     return config
  //   }
  // }
})

export { cjRequest, cjRequest2 }
