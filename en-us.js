module.exports = {
  admin: 'admin',
  // 查看基础信息相关
  number: 'No',
  name: 'name',
  createdUserName: 'createdUserName',
  lastUpdateUserName: 'lastUpdateUserName',
  createdDateTime: 'createdDateTime',
  lastUpdateDateTime: 'lastUpdateDateTime',
  comments: 'comments',
  type: 'type',
  typeResume: 'typeResume',
  date: 'date',
  startDate: 'startDate',
  endDate: 'endDate',
  resume: 'resume',

  // 表单常见操作
  detail: 'detail',
  edit: 'edit',
  submit: 'submit',
  confirm: 'confirm', // 所有的确认  都改成 确定
  sort: 'sort',
  cancel: 'cancel',
  back: 'back',
  delete: 'delete',
  close: 'close',
  add: 'add',
  new: 'new',
  batchDelete: 'batchDelete',
  save: 'save',
  saveCommit: 'save&Commit',
  export: 'export',
  download: 'download',
  all: 'all',

  // 常见提示
  loadingInfo: 'loading...',
  optionSuccess: 'option success',
  optionFailed: 'option failed, please contact your system administrator',
  fileDownloading:
    'The file has started to download. Please pay attention to the information at the bottom of your browser',
  deleteConfirm: 'Confirm that you want to delete this data ?',
  inputPlaceHolder: 'Please enter a name',
  selectByAccount: 'Please select personnel according to account name',

  // 查询相关
  search: 'search',
  reset: 'reset',

  // 表格操作相关
  view: 'view',
  action: 'action',

  // 员工信息
  userNo: 'userNo',
  realName: 'realName',
  account: 'account',
  enName: 'enName',
  mail: 'mail',
  status: 'status',
  normal: 'normal',
  inService: 'inService',
  outService: 'outService',
  firstName: 'firstName',
  givenName: 'givenName',
  companyName: 'companyName',
  departmentName: 'departmentName',
  position: 'position',
  site: 'site',
  area: 'area',
  office: 'office',
  directLid: 'directLid',
  directLidNo: 'directLidNo',
  directLidName: 'directLidName',
  enterDate: 'enterDate',
  leaveDate: 'leaveDate',

  // `total ${total}`
  //'pagination: `total ${total}`',
  pagination: ['total', 'record'],
  currency: '$',

  // 资产相关通用参数
  viewInfo: 'viewInfo',
  inventorySite: 'inventorySite',
  staff: 'staff',
  rollOutSite: 'rollOutSite',
  rollInSite: 'rollInSite',
  selectSite: 'selectSite',
  staffName: 'staffName',
  selectInfo: 'Optional type',
  assetsInputPlaceHolder: 'Input criteria select the item',
  receiveItem: 'receiveItem',
  assetsReceiveInfo: 'Recipients (no. | items name)',
  receiveAmount: 'receiveAmount',
  receiverNo: 'receiverNo',
  receiverName: 'receiverName',
  receiveSite: 'receiveSite',
  receiveReason: 'receiveReason',
  receiveDate: 'receiveDate',
  assetsNo: 'assetsNo',

  // 各个菜单页面标签
  ldap: {
    ou: {
      index: {
        newMenu: 'newMenu',
        companyDepartment: 'companyDepartment',
        newSubGroup: 'newSubGroup',
        newGroup: 'newGroup',
        selectCreateDirectory: 'Select the location to create a new directory',
        fillRequiredInformation:
          'Please fill in the required information of your account'
      },
      addOu: {
        // addOu
        applyDn: 'applyDn',
        autoGenerate: 'autoGenerate',
        menuName: 'menuName',
        editProxyGroup: 'editProxyGroup',
        newProxyGroup: 'newProxyGroup'
      }
    },
    user: {
      index: {
        pleaseSelectStatus: 'please select status',
        newUser: 'newUser',
        forbidConfirm: 'Confirm that you want to disable the user?',
        forbid: 'forbid',
        usingConfirm: 'Confirm that you want to active the user?',
        using: 'active',
        resetConfirm: 'Confirm that you want to reset the user password ?',
        resetPassword: 'reset password',
        memberOf: 'memberOf',
        dnManager: 'dnSuperManager',
        deleteWarning:
          'The data cannot be restored after being deleted. Confirm to delete the data ?'
      },
      addGroupsModal: {
        addProxyGroup: 'addProxyGroup',
        inputPlaceHolder: 'Please enter a name'
      },
      addParentsModal: {
        addProxyGroup: 'addProxyGroup',
        inputPlaceHolder: 'Please enter a name'
      },
      addUser: {
        selectCreateDirectory:
          'Select the location where you want to create the proxy group',
        necessaryInfo:
          'Please fill in the required information of your account',
        applyDn: 'applyDn',
        autoGenerate: 'autoGenerate',
        cnName: 'cnName',
        cnUniqueInfo: 'Under the same organization must be unique',
        domainAccount: 'domainAccount',
        domainUniqueInfo: 'The domain must be unique，ex: sanfeng.zhang',
        scriptName: 'scriptName',
        consistencyInfo:
          'If the value is null, it is the same as the domain account by default,ex: sanfeng_zhang.bat',
        department: 'department',
        selectDepartment: 'Please select department',
        company: 'company',
        selectCompany: 'Please select company',
        selectPosition: 'Please select position',
        site: 'site',
        selectType: 'Please select type',
        directLid: 'directLid',
        selectByAccount: 'Please select personnel according to account name',
        confirmInfo: 'Please select personnel according to account name',
        enName: 'enName',
        enShort: 'enShort',
        editUser: 'editUser',
        addUser: 'addUser'
      },
      addUsersModal: {
        addUser: 'addUser',
        status: 'status'
      },
      detailView: {
        viewInfo: 'View function role information',
        scriptName: 'scriptName',
        positionRecord: 'Job records',
        domainDn: 'domainDn'
      },
      memberOf: {
        proxyGroup: 'proxyGroup',
        deleteErrorEmpty: 'The deleted object cannot be empty'
      },
      roleUser: {
        role: 'Role user',
        addRole: 'addRole',
        addProxyGroup: 'addProxyGroup',
        member: 'member',
        proxyGroup: 'proxyGroup',
        status: 'status',
        deleteErrorEmpty: 'The deleted object cannot be empty'
      }
    },
    groups: {
      addGroup: {
        selectCreateDirectory:
          'Select the location where you want to create the proxy group',
        necessaryInfo:
          'Please fill in the required information of your account',
        applyDn: 'applyDn',
        autoGenerate: 'autoGenerate',
        editProxyGroup: 'editProxyGroup',
        newProxyGroup: 'newProxyGroup'
      },
      addGroupsModal: {
        addProxyGroup: 'addProxyGroup'
      },
      addParentsModal: {
        addProxyGroup: 'addProxyGroup'
      },
      addUsersModal: {
        addUser: 'addUser',
        accountInput: 'Please enter a account',
        nameInput: 'Please enter name',
        status: 'status'
      },
      detailView: {
        viewInfo: 'view the information',
        roleName: 'roleName'
      },
      index: {
        inputDn: 'Please enter dn',
        direction: 'direction',
        folderInput: 'Please enter folder',
        typeSelect: 'Please select type',
        isProject: 'ProjectOrNot',
        notProject: 'notProject',
        newProxyGroup: 'newProxyGroup',
        memberOf: 'memberOf',
        member: 'member',
        setAsProject: 'setAsProject',
        dnAdmin: 'dnSuperAdmin',
        deleteWarning:
          'The data cannot be restored after being deleted. Confirm to delete the data ?',
        readOnly: 'bind readonly proxy',
        readWrite: 'bind read and write proxy',
        OP: 'bind OP proxy',
        LP: 'bind LP proxy',
        unbind: 'unbind',
        boundReadOnly: 'bound read only',
        boundReadWrite: 'bound read and write',
        bindingFailed: 'binding failed',
        bindingSuccess: 'binding success',
        boundLp: 'bound Lp proxy',
        boundOp: 'bound Op proxy',
        editDirection: 'edit direction'
      },
      memberOf: {
        proxyBelong: 'proxyBelong',
        inputDn: 'Please enter dn group',
        deleteErrorEmpty: 'The deleted object cannot be empty'
      },
      roleUser: {
        roleUser: 'roleUser',
        inputDn: 'Please enter dn group',
        addUser: 'addUser',
        addProxyGroup: 'addProxyGroup',
        member: 'member',
        proxyGroup: 'proxyGroup',
        status: 'status',
        deleteErrorEmpty: 'The deleted object cannot be empty'
      }
    }
  },
  employee: {
    selectStatus: 'Please select a status',
    selectType: 'Please select a type',
    exportMail: 'exportMail',
    exportMember: 'exportMember',
    pagination: 'pagination',
    serviceUser: 'serviceUser',
    employeeInfo: 'employeeInfo',
    combineMail: 'combineMail'
  },
  apply: {
    edit: {
      summitConfirm: 'sure to save and submit current form?',
      saveCommit: 'save and submit current form',
      saveForm: 'save current form',
      saveDraft: 'saveDraft',
      select: 'please select',
      formIllegalInfo: [
        'Form line ',
        ': without a “Name”，should not have“',
        '”data',
        'line ',
        'file direction error：the ',
        'character of the data is a special character'
      ],
      formEmptyInfo: [
        'Form line',
        '',
        "data can't be empty",
        'form info: “',
        "can't be empty"
      ],
      formItemEmptyInfo: 'The application form cannot be empty'
    },
    formTag: {
      waitingYou: 'waitingYou',
      waitingOther: 'waitingOther',
      completed: 'completed',
      loseEfficacy: 'loseEfficacy',
      draft: 'draft'
    },
    historyTag: {
      operationRecord: 'operationRecord',
      since: 'since',
      complete: 'complete',
      overdue: 'overdue',
      draft: 'draft',
      sendBack: 'sendBack',
      backReason: '---backReason',
      sign: 'sign'
    },
    list: {
      formName: 'formName',
      startDate: 'startDate',
      isProject: 'ProjectOrNot',
      notProject: 'notProject',
      project: 'project',
      isDeployed: 'isDeployed',
      unDeployed: 'unDeployed',
      deployed: 'deployed',
      commitFailInfo: [
        'Submission failed. The current application has been saved to the draft page',
        'cause of failure'
      ],
      rejectInfo: ['file："', '”back success'],
      draftDeleteSuccess: 'draft delete success'
    },
    tagItem: {
      draftFile: 'draftFile',
      viewFormHistory: 'View the form and its history information',
      editForm: 'Edit current form',
      commitConfirm: 'confirm submit the form?',
      submitForm: 'submit current form',
      deleteConfirm: 'confirm delete the form?',
      deleteFile: 'delete form',
      waitingYouSign: 'The document is for your signature',
      waiting: 'ToBeSigned',
      signedTitle:
        'The document has been signed. Click to view the signature record',
      signed: 'signed',
      downloadFile: 'downloadFile',
      backToApply: 'backToApply',
      sendBack: 'sendBack',
      deployConfirm: 'confirm to deploy?',
      deployTitle:
        'You are the IT Audit Administrator. Click Complete Deployment',
      finishDeploy: 'finishDeploy',
      sendMailConfirm: 'confirm to send mail?',
      sendMailInfo: 'You are the IT audit administrator, click Send email',
      sendMail: 'sendMail',
      sendAgainConfirm: 'You have sent the email, confirm to send it again?',
      signSite: 'Signed by',
      directionIllegal: 'The application folder contains non-project paths',
      projectDirection: 'The application folder is the project path',
      notProject: 'notProject',
      isProject: 'ProjectOrNot',
      deployed: 'deployed',
      unDeployed: 'unDeployed',
      backReason: 'backReason',
      draft: 'draft',
      completed: 'completed',
      loseEfficacy: 'loseEfficacy',
      overdue: 'overdue',
      singing: 'singing',
      createInfo: ['from ', ' since ', ' create', ' start'],
      waitYouToSign: '-For you to sign',
      waitOtherToSign: '-For other to sign（ ',
      itAuditor: 'IT Audit Administrator'
    }
  },
  callRecord: {
    formList: {
      signPerson: 'Signed by: Official number or name',
      targetClient: 'Target user: name or email address',
      proxyDirection: 'proxyDirection',
      applyDate: 'applyDate',
      isProject: 'ProjectOrNot',
      notProject: 'notProject',
      project: 'project',
      draftFile: 'draftFile',
      viewFormHistory: 'View the form and its history information',
      downloadFile: 'downloadFile',
      signSite: 'Signed by',
      directionIllegal: 'The application folder contains non-project paths',
      projectDirection: 'The application folder is the project path',
      deployed: 'deployed',
      unDeployed: 'unDeployed',
      backReason: 'backReason',
      draft: 'draft',
      completed: 'completed',
      loseEfficacy: 'loseEfficacy',
      overdue: 'overdue',
      singing: 'singing',
      createInfo: ['from ', ' create', ' start'],
      signInfo: ['-For you to sign', '-For other to sign（ ']
    },
    recordDetail: {
      subject: 'subject',
      projectDir: 'projectDir',
      dnGroup: 'dnGroup',
      curStatus: 'curStatus',
      formRecordDetail: 'formRecordDetail'
    },
    recordList: {
      formName: 'formName',
      unSet: 'unSet',
      success: 'success',
      fail: 'fail',
      noDnGroup: 'noDnGroup',
      exception: 'exception',
      viewFormTitle: 'Click to see the form details',
      viewSubTaskTitle: 'Click to view subtask information',
      subtask: 'subtask',
      applyForm: 'applyForm',
      direction: 'direction',
      dnGroup: 'dnGroup'
    },
    subTasks: {
      unSet: 'unSet',
      success: 'success',
      fail: 'fail',
      connected: 'beenAssociated',
      exception: 'exception',
      direction: 'direction',
      subtaskInfo: 'subtaskInfo'
    },
    list: {
      useRecord: 'useRecord',
      formRecord: 'formRecord'
    }
  },
  assets: {
    consume: {
      detailView: {
        outAmount: 'Number of spending',
        outReason: 'Reason of spending'
      },
      edit: {
        assets: 'assets',
        inventoryAmount: 'inventoryAmount',
        outAmount: 'Number of spending',
        outReason: 'Reason of spending',
        outDate: 'Date of spending',
        inventoryAmountExceed:
          'The amount of expenditure must not be greater than the current inventory！'
      },
      list: {
        selectType: 'Please select a type',
        staffType: 'staffType',
        inventoryLocation: 'inventoryLocation',
        outStaff: 'Spending items',
        outAmount: 'Number of spending',
        outReason: 'Reason of spending'
      }
    },
    consumeTransfer: {
      detailView: {
        transferAmount: 'transferAmount',
        transferReason: 'transferReason'
      },
      edit: {
        transferConsumableItem: 'transferConsumableItem',
        inventoryAmount: 'inventoryAmount',
        transferAmount: 'transferAmount',
        transferReason: 'transferReason',
        transferDate: 'transferDate',
        inventoryAmountExceed:
          'The amount of expenditure must not be greater than the current inventory！',
        locationSameError:
          'The place of transfer is not the same as the place of transfer！'
      },
      list: {
        staffType: 'staffType',
        outLocation: 'outLocation',
        inLocation: 'inLocation',
        transferStaff: 'transferStaff',
        outAmount: 'Number of spending',
        outReason: 'Reason of spending'
      }
    },
    detailed: {
      detailView: {
        brand: 'brand',
        type: 'type',
        configure: 'configure',
        leisure: 'leisure',
        scrap: 'scrap',
        lost: 'lost',
        receiver: 'receiver',
        isException: 'ExceptionOrNot'
      },
      edit: {
        receiveType: 'receiveType',
        personalReceive: 'personalReceive',
        publicReceive: 'publicReceive',
        receiver: 'receiver',
        selectStaff: 'selectStaff',
        addStaff: 'addStaff',
        assetsRepeated: 'Duplicate content exists in the selected asset！'
      },
      list: {
        configure: 'configure',
        staffConfigure: 'staffConfigure',
        number: 'number',
        receiveState: 'receiveState',
        leisure: 'leisure',
        personalReceive: 'personalReceive',
        publicReceive: 'publicReceive',
        scrap: 'scrap',
        lost: 'lost',
        selectType: 'Please select a type',
        allSite: 'allArea',
        isBroken: 'BrokenOrNot',
        broken: 'broken',
        allType: 'allType',
        firstReceiveDate: 'firstReceiveDate',
        repair: 'repair',
        user: 'user',
        useSite: 'useSite',
        backTo: 'backTo',
        backDate: 'backDate',
        personalDuty: 'personalDuty',
        unitDuty: 'unitDuty',
        dutySite: 'dutySite',
        personLiable: 'personLiable',
        lostDate: 'lostDate',
        receiverOrSite: 'receiver / site',
        site: 'site',
        editException: 'editException',
        back: 'Items returned',
        assetsList: 'assetsList_'
      }
    },
    goods: {
      edit: {
        identificationWarning:
          "Name should be as recognizable as possible, such as :' Lenovo T490', 'Dell display 19 inch'",
        brand: 'brand',
        type: 'type',
        configure: 'configure',
        assetsProperties: 'assetsProperties',
        selectType: 'selectType'
      },
      list: {
        configure: 'configure',
        brand: 'brand',
        type: 'type',
        properties: 'properties'
      }
    },
    goodsCaseTransfer: {
      detailView: {
        transferAssetsAmount: 'transferAssetsAmount'
      },
      edit: {
        transferReason: 'transferReason',
        transferDate: 'transferDate',
        addStaff: 'addStaff',
        assetsRepeated: 'Duplicate content exists in the selected asset！'
      },
      list: {
        assetsInfo: 'Asset number or item name',
        transferReason: 'transferReason'
      }
    },
    into: {
      detailView: {
        brand: 'brand',
        type: 'type',
        configure: 'configure',
        purchaseType: 'purchaseType',
        supplier: 'supplier',
        onlineRetailers: 'onlineRetailers',
        offLine: 'offLine',
        purchaseDate: 'purchaseDate',
        purchaseSite: 'purchaseSite',
        handledBy: 'handledBy',
        purchaseLink: 'purchaseLink'
      },
      edit: {
        assets: 'assets',
        properties: 'properties',
        purchaseSite: 'purchaseSite',
        unitPrice: 'unitPrice',
        amount: 'amount',
        assetsAmountErr:
          'When an item is an asset, the number cannot be greater than 20',
        totalPrice: 'totalPrice',
        assetsInputHelper:
          'If the number of assets is not more than 20, separate the numbers of assets with commas (,) and do not use enter or space ',
        purchaseType: 'purchaseType',
        supplier: 'supplier',
        onlineRetailers: 'onlineRetailers',
        offLine: 'offLine',
        purchaseLink: 'purchaseLink',
        purchaseDate: 'purchaseDate',
        ITHandledBy: 'ITHandledBy',
        assetsConsistencyError:
          'The number of assets is inconsistent with the number of articles'
      },
      list: {
        handledBy: 'handledBy',
        namePlaceHolder: 'Enter name',
        staffPlaceHolder: 'Enter item name',
        configure: 'configure',
        configurePlaceHolder: 'Enter configure',
        noPlaceHolder: 'Enter number',
        selectSite: 'Please select a region',
        staffProperties: 'Item properties',
        selectProperties: 'Please select a properties',
        totalExpenditureAmount: 'Total amount of expenditure',
        purchaseDate: 'purchaseDate',
        properties: 'properties',
        assets: 'assets',
        consumableItem: 'consumableItem',
        amount: 'amount',
        unitPrice: 'unitPrice',
        totalPrice: 'totalPrice',
        purchaseType: 'purchaseType',
        supplier: 'supplier',
        onlineRetailers: 'onlineRetailers',
        offLine: 'offLine',
        purchaseStaff: 'purchaseStaff'
      }
    },
    lose: {
      edit: {
        selectAssets: 'selectAssets',
        scrapDate: 'scrapDate'
      },
      list: {
        personLiable: 'personLiable',
        dutySite: 'dutySite',
        selectType: 'Please select a type',
        staffNo: 'item number',
        dutyType: 'dutyType',
        personalDuty: 'personalDuty',
        unitDuty: 'unitDuty',
        personLiableOrUnit: 'personLiable / Unit'
      }
    },
    offer: {
      detailView: {
        receiveType: 'receiveType',
        personalReceive: 'personalReceive',
        publicReceive: 'publicReceive',
        receiver: 'receiver'
      },
      edit: {
        selectAssets: 'selectAssets',
        addStaff: 'addStaff',
        staffRepeated: 'Duplicate content exists in the selected asset！'
      },
      list: {
        assetsInfo: 'assetsInfo',
        assetsName: 'Asset number or item name',
        personalReceive: 'personalReceive',
        publicReceive: 'publicReceive',
        receiverOrSite: 'receiver / site',
        receiveType: 'receiveType'
      }
    },
    plan: {
      detailView: {
        viewDetailInfo: 'viewDetailInfo',
        planDate: 'planDate',
        subject: 'subject',
        content: 'content',
        completeDate: 'completeDate'
      },
      edit: {
        planDate: 'planDate',
        subject: 'subject',
        content: 'content'
      },
      list: {
        subject: 'subject',
        content: 'content',
        completed: 'completed',
        unCompleted: 'unCompleted',
        planDate: 'planDate',
        completeDate: 'completeDate',
        plan: 'fulfilment of plan',
        receiver: 'receiver',
        backToPersonNo: 'backToPersonNo',
        backPerson: 'backPerson',
        backSite: 'backSite'
      }
    },
    record: {
      detailView: {
        leadOutSite: 'leadOutSite'
      },
      list: {
        employeeName: 'employeeName',
        recordType: 'recordType',
        personalReceive: 'personalReceive',
        groupReceive: 'groupReceive',
        personalBack: 'personalBack',
        unitBack: 'unitBack',
        scrap: 'scrap',
        lost: 'lost',
        transfer: 'transfer',
        relationSite: 'relationSite',
        publicReceive: 'publicReceive',
        user: 'user',
        useSite: 'useSite',
        backTo: 'backTo',
        backDate: 'backDate',
        flowType: 'flowType',
        lendSite: '领出 / 归还方',
        receiveSite: 'Claim/return party',
        backAssets: 'backAssets',
        assetsFlowRecord: 'MovementRecord'
      }
    },
    scrap: {
      edit: {
        selectAssets: 'selectAssets',
        scrapDate: 'scrapDate'
      },
      list: {
        relationSite: 'relationSite',
        site: 'site',
        assetsNo: 'assetsNo'
      }
    },
    stock: {
      detailView: {
        leisure: 'leisure',
        personalReceive: 'personalReceive',
        publicReceive: 'publicReceive',
        scrap: 'scrap',
        viewDetail: 'viewDetail',
        back: 'back',
        inventory: 'inventory',
        brand: 'brand',
        type: 'type'
      },
      edit: {
        receiveType: 'receiveType',
        personalReceive: 'personalReceive',
        publicReceive: 'publicReceive',
        receiver: 'receiver',
        selectStaff: 'selectStaff',
        addStaff: 'addStaff',
        assetsRepeated: 'Duplicate content exists in the selected asset！'
      },
      list: {
        location: 'location',
        selectType: 'Please select a type',
        leisure: 'leisure',
        personalReceive: 'personalReceive',
        publicReceive: 'publicReceive',
        scrap: 'scrap',
        back: 'back',
        useSite: 'useSite',
        backTo: 'backTo',
        backDate: 'backDate',
        inventory: 'inventory',
        backStaff: 'backStaff',
        inventoryStaff: 'inventoryStaff'
      }
    },
    type: {
      edit: {
        typeName: 'typeName',
        resume: 'resume',
        assetsType: 'assetsType'
      },
      list: {
        typeName: 'typeName',
        typePlaceHolder: 'add type'
      }
    }
  },
  system: {
    group: {
      addGroup: 'NewAssessmentGroup',
      isSubmit: 'isSubmit ?',
      editGroup: 'editGroup'
    },
    holiday: {
      detailView: {
        viewInfo: 'viewInfo',
        holiday: 'holiday',
        restDay: 'restDay',
        week: 'week'
      },
      edit: {
        selectType: 'Please select status',
        restDay: 'restDay',
        holiday: 'holiday'
      },
      list: {
        selectType: 'Please select status',
        restDay: 'restDay',
        holiday: 'holiday'
      }
    },
    month: {
      edit: {
        assessmentMonth: 'assessmentMonth',
        selectMonth: 'Please select the month of assessment'
      },
      list: {
        monthlyPerformance: 'monthlyPerformance',
        closed: 'post-project',
        used: 'already used',
        unused: 'unused',
        useConfirm: 'confirm to use?',
        closeConfirm: 'confirm to post-project?',
        assessmentCycle: 'assessmentCycle'
      }
    },
    projectManage: {
      addProjectProxy: {
        personLiableInfo: 'personLiableInfo',
        employeeEmpty: 'Personnel information cannot be empty',
        projectDir: 'projectDir',
        projectFolder: 'projectFolder',
        projectAlias: 'projectAlias',
        serviceType: 'serviceType',
        selectServiceType: 'Select a service type',
        serviceTypeEmpty: 'The service type cannot be empty',
        projectProxy: 'projectProxy',
        correspondingRelationship: 'relationship'
      },
      deList: {
        projectFolder: 'projectFolder',
        projectAlias: 'projectAlias',
        addCorrespondingRelationship: 'relationship',
        serviceType: 'serviceType'
      },
      spList: {
        projectDir: 'projectDir',
        selectServiceType: 'Select a service type',
        serviceTypeEmpty: 'The service type cannot be empty',
        addSPProxy: 'addSPProxy',
        serviceType: 'serviceType'
      }
    },
    projectPerson: {
      addPersonProxy: {
        employeeInfo: 'employeeInfo',
        employeeEmpty: 'Personnel information cannot be empty!',
        role: 'role',
        isUse: 'useOrNot',
        activate: 'activate',
        deactivate: 'deactivate',
        priorityLevel: 'priorityLevel',
        newProxy: 'NewPersonnelRights',
        viewProxy: 'viewPersonnelRights',
        editProxy: 'editPersonnelRights'
      },
      list: {
        newProxy: 'NewPersonnelRights',
        activeConfirm: 'Confirm the current role is enabled?',
        activate: 'activate',
        activateInfo:
          'If the current role is enabled, click To disable the current role',
        deactivateConfirm: 'Confirm to disable the current role?',
        priorityLevel: 'priorityLevel'
      }
    },
    push: {
      edit: {
        ignoreField: 'ignoreField',
        resume: 'resume'
      },
      index: {
        addConfigure: 'addConfigure',
        postDirection: 'postDirection',
        ignoreField: 'ignoreField',
        resume: 'resume'
      }
    },
    resource: {
      edit: {
        parentMenu: 'parentMenu',
        parentMenuEmptyWarning: 'You must select the parent menu first',
        parentMenuName: 'parentMenuName',
        routerName: 'routerName',
        applyDir: 'applyDir',
        redirect: 'redirect',
        hideSubMenu: 'hideSubMenu',
        emptyWarning: 'can not be empty'
      },
      index: {
        addSubMenu: 'addSubMenu',
        resource: 'resource',
        routerName: 'routerName',
        applyDir: 'applyDir',
        redirect: 'redirect',
        selectParentMenu: 'Please select the parent menu first'
      }
    },
    role: {
      add: {
        addRole: 'roleName',
        namePlaceHolder: 'Enter a role name',
        userPlaceHolder: 'Enter a user name',
        lengthRemind55: 'A maximum of 55 characters！',
        resumePlaceHolder: 'Please enter a description!',
        lengthRemind105: 'A maximum of 105 characters！',
        editRole: 'editRole',
        entryRole: 'entryRole'
      },
      addUsersModal: {
        addUser: 'addUser',
        status: 'status'
      },
      detailView: {
        viewInfo: 'View function role information',
        roleName: 'roleName',
        lastUpdateDateTime: 'lastUpdateDateTime'
      },
      limits: {
        proxyConfig: 'proxyConfig'
      },
      list: {
        selectPlaceHolder: 'Please select status',
        waitToSubmit: 'To submit',
        submitted: '已提交',
        addRole: 'submitted',
        proxyConfig: 'proxyConfig',
        roleUser: 'roleUser',
        deleteConfirm: 'Are you sure you want to delete this data'
      },
      roleUser: {
        roleUser: 'roleUser',
        addUser: 'addUser',
        status: 'status',
        deleteWarning: 'The deleted object cannot be empty'
      }
    }
  },
  service: {
    enlargeModal: {
      itemImporting: 'item Importing...',
      cpuUseRecord: 'CPU Usage Record（percentage）',
      useLv: 'useLv',
      used: 'used',
      leisure: 'leisure',
      memoryStatus: 'memoryStatus',
      memory: 'memory: ',
      changeToLine: 'changeToLine',
      diskStatus: 'diskStatus',
      proportion: 'proportion',
      memoryUseRecord: 'memoryUseRecord（percentage）',
      changeToPie: 'changeToPie',
      enlarge: 'enlarge'
    },
    index: {
      inputIpInfo: 'Enter the server IP address',
      serviceType: 'serviceType',
      filterTime: 'Record filter time',
      recordTime: 'recordTime'
    },
    serviceItem: {
      cpuUseRecord: 'CPU Usage Record（percentage）',
      useLv: 'useLv',
      enlarge: 'enlarge',
      used: 'used',
      leisure: 'leisure',
      memoryStatus: 'memoryStatus',
      memory: 'memory',
      changeToLine: 'changeToLine',
      diskStatus: 'diskStatus',
      proportion: 'proportion',
      CDeskRecord: 'CDeskRecord',
      memoryUseRecord: 'memoryUseRecord（percentage）',
      changeToPie: 'changeToPie'
    }
  },
  user: {
    login: {
      loginFailedInfo: 'The account or password is incorrect',
      accountDemo: 'account，wx：sanfeng.zhang',
      accountPlaceholder: 'Please enter an account name',
      passwordPlaceholder: 'Please enter an password',
      forgetPassword: 'forgetPassword',
      verifyCodeSending: 'verifyCodeSending..',
      tips: 'tips',
      verifySuccessInfo:
        'The verification code is successfully obtained. Your verification code is',
      welcome: 'welcome',
      welcomeBack: 'welcomeBack',
      retry: 'An error occurred. Please try again later'
    },
    register: {
      register: 'register',
      mailPlaceholder: 'Please enter your email address',
      strength: 'strength',
      passwordRule:
        'Please enter at least 6 characters.  Do not use passwords that can be easily guessed. ',
      passwordErrorInfo:
        'A password of at least six characters is case sensitive',
      ensurePassword: 'confirm password',
      phoneNum: '11 digit cell phone number',
      phoneNumWarning: 'Please enter the correct phone number',
      verifyPlaceHolder: '请输入验证码',
      achieveVerifyCode: 'Please enter the verification code',
      alreadyLogin: 'Log in using an existing account',
      low: 'low',
      mid: 'mid',
      high: 'strong',
      strengthError: 'Insufficient password strength',
      passwordPlaceholder: 'Please enter an password',
      passwordConsistencyError: 'The passwords are inconsistent',
      verifyCodeSending: 'verifyCodeSending..',
      tip: 'tips',
      verifySuccessInfo:
        'The verification code is successfully obtained. Your verification code is',
      error: 'error',
      retry: 'An error occurred. Please try again later'
    },
    registerResult: {
      viewMail: 'check out mailbox',
      backIndex: 'back homepage',
      registerSuccess: 'your account：${v} register success',
      activeMailInfo:
        'The activation email has been sent to your email and is valid for 24 hours.  Please log in your email and click the link in the email to activate your account. '
    },
    resetPassword: {
      passwordPlaceholder: 'Enter a new password',
      strength: 'strength',
      passwordRule:
        '请输入至少9位字符，新密码须同时包含大、小写字母及数字三种字符。',
      passwordError:
        'Enter at least 9 characters. The new password must contain lowercase letters, uppercase letters, and digits ',
      fixedSuccess: 'fixedSuccess',
      resetSuccessInfo:
        'Your password has been reset successfully, please click ok to return to the login page',
      low: 'low',
      mid: 'mid',
      high: 'strong',
      emptyWaring: 'The password cannot be empty',
      lengthWarning: 'Password length not enough',
      strengthWarning: 'Insufficient password strength',
      passwordConsistencyError: 'The passwords are inconsistent',
      verifySend:
        'The verification code has been sent to your email address. Please fill in and submit it within 3 minutes',
      tip: 'tips',
      verifySendError: 'Failed to send verification code.',
      error: 'error',
      retry: 'An error occurred. Please try again later'
    },
    sso: {
      progress: 'Login progress',
      ssoLoading: 'ssoLoading...',
      achieveToken: 'Detection for PC login, obtain ltpaToken ...',
      ssoLoadingInfo: 'LtpaToken has been obtained. Login is in progress...',
      ssoFieldInfo: 'Failed to get ltpaToken. Login failed ',
      turning: 'If the verification succeeds, the page is displayed...',
      welcome: 'welcome',
      welcomeBack: 'welcomeBack',
      error: 'error',
      retry: 'An error occurred. Please try again later'
    },
    verifyMailCode: {
      mailVerify: 'mailbox verification',
      accountDemo: 'account，ex：sanfeng.zhang',
      accountTitle: 'Please enter a account',
      mailInputTitle: 'Please enter your email address',
      verifyCode: 'verifyCode',
      verifyPlaceholder: 'Please enter the verification code',
      achieveVerifyCode: 'Getting Verification Code',
      backLogin: 'Return to the login',
      fixedSuccess: 'modify successfully',
      resetSuccessInfo:
        'Your password has been reset successfully, please click ok to return to the login page',
      low: 'low',
      mid: 'mid',
      high: 'strong',
      emptyError: 'The password cannot be empty',
      lengthError: 'Password length not enough',
      strengthError: 'Insufficient password strength',
      passwordPlaceholder: ' Please enter your password',
      passwordConsistencyError: 'The passwords are inconsistent',
      tips: 'tips',
      verifySendInfo:
        'Verification code has been sent to your email, please fill in and submit within 3 minutes. ',
      verifyCodeSendError: 'Description Failed to send the verification code，',
      error: 'error',
      retryWarning: 'An error occurred. Please try again later'
    }
  },
  settingDrawer: {
    settingConfig: {
      changingSubject: 'Switching themes！'
    },
    settingDrawer: {
      globalStyleConfig: 'globalStyleConfig',
      darkStyle: 'darkStyle',
      lightStyle: 'lightStyle',
      subjectColor: 'subjectColor',
      navigationMode: 'NavigationMode',
      sideMenu: 'sideBarNavigation',
      topMenu: 'TopBarNavigation',
      proxyDepends: 'This setting is valid only for top bar navigation',
      fixed: 'fixed',
      flex: 'flex',
      contentWidth: 'contentWidth',
      fixedHeader: 'fixedHeader',
      fixedHeaderConfigTitle:
        'This parameter can be configured if the Header is fixed',
      hideHeaderInfo: 'Hide the Header while sliding',
      fixedSideMenu: 'fixedSideMenu',
      otherSetting: 'otherSetting',
      achromaticMode: 'Color blindness mode',
      tabsMode: 'Multi-tab mode',
      sideConfigInfo:
        'The configuration bar is used for preview only in the development environment and is not displayed in the production environment. Therefore, manually modify the configuration file.  After modifying the configuration file, clear the local cache and LocalStorage ',
      copyCompleted: 'copyCompleted',
      copyField: 'copyField'
    }
  },
  globalFooter: {
    footerInfo: '南京艾科曼信息技术有限公司'
  },
  table: {
    selected: 'selected'
  },
  tools: {
    logo: {
      projectName: 'IT Management system'
    },
    UserMenu: {
      exit: 'exit'
    }
  },
  userLayout: {
    projectName: 'IT Management system',
    CopyrightInfo: ['Copyright & copy', '南京艾科曼信息技术有限公司']
  },
  domUtil: {
    projectName: 'IT Management system'
  },
  request: {
    error: 'error',
    reloadWarning: 'Failed to request user information. Please log in again'
  },
  permission: {
    error: 'error',
    reloadWarning: 'Failed to request user information. Please try again'
  },
  exceptionPage: {
    backIndex: 'back homepage',
    errorInfo403: 'Sorry, you have no access to this page',
    errorInfo404:
      'Sorry, the page you visited does not exist or is still under development',
    errorInfo500: 'Sorry, server error'
  }
}
